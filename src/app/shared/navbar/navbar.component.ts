import {Component, OnInit, DoCheck} from '@angular/core';
import {AuthService} from "../../auth/services/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
  providers: [AuthService]
})
export class NavbarComponent implements OnInit, DoCheck {
  public isLogged = false;
  public user: any;

  constructor(private authSvc: AuthService, private router: Router) {
  }

  async ngOnInit() {
    await this.statusUser()
  }

  async ngDoCheck() {
    await this.statusUser()
  }

  async onLogout() {
    await this.authSvc.logOut();
    await this.router.navigate(['/login']);
  }

  async statusUser() {
    this.user = await this.authSvc.getCurrentUser();
    this.isLogged = !!this.user;
  }

}
