export interface  User {
  name: string,
  document: number,
  email: string,
  age: number
}
