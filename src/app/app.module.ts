import {NgModule} from '@angular/core';
import {CommonModule} from "@angular/common";

import {BrowserModule} from '@angular/platform-browser';
import {AppComponent} from './app.component';
import {Routes, RouterModule} from "@angular/router";
import {AppRoutingModule} from './app-routing.module';
import {NavbarComponent} from "./shared/navbar/navbar.component";
import {ReactiveFormsModule} from "@angular/forms"
import {AngularFireModule} from "@angular/fire/compat";
import {AngularFireAuthModule} from "@angular/fire/compat/auth";
import {environment} from "../environments/environment";

const routes: Routes = [];

@NgModule({
  declarations: [
    AppComponent, NavbarComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    CommonModule,
    AppRoutingModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule
  ],
  providers: [],
  exports: [NavbarComponent],
  bootstrap: [AppComponent]
})
export class AppModule {
}
