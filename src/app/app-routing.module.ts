import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from "@angular/router";

const rotes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  }, {
    path: 'login', loadChildren: () => import('./auth/login/login.module').then(m => m.LoginModule)
  }, {
    path: 'home', loadChildren: () => import('./home/home.module').then(m => m.HomeModule)
  }, {
    path: 'users', loadChildren: () => import('./users/users.module').then(m => m.UsersModule)
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(rotes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
