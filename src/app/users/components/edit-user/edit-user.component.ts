import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {
  userForm = this.fb.group({
    name: '',
    document: '',
    email: '',
    age: ''
  });
  value: { [p: string]: any } | undefined;
  private isEmail!: '/\S+@\S+\.\s+/';

  constructor(private router: Router, private fb: FormBuilder) {
    const navigation = this.router.getCurrentNavigation();
    this.value = navigation?.extras?.state;
    console.log(this.value);
  }

  ngOnInit(): void {
  }

  onSubmit(): void {
    // @ts-ignore
    console.log('Saved', this.userForm.value)
  }
}
