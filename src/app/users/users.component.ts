import {Component, OnInit} from '@angular/core';
import {NavigationExtras, Router} from "@angular/router";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  navigationExtras: NavigationExtras = {
    state: {
      value: null
    }
  }

  users = [
    {
      name: 'Sebastian Masmela',
      document: 1022431903,
      email: 'elsebatian14@outlook.es',
      age: 23
    }, {
      name: 'Felipe Garcia',
      document: 1234567890,
      email: 'felipegarcia1806@gmail.com',
      age: 22
    }
  ];

  constructor(private router: Router) {
  }

  ngOnInit(): void {
  }

  newUser(): void {

  }

  editUser(item: any): void {
    // @ts-ignore
    this.navigationExtras.state.value = item;
    this.router.navigate(['/users/edit'], this.navigationExtras);
  }
}
