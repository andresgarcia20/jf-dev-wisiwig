import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {UsersComponent} from './users.component';
import {EditUserComponent} from './components/edit-user/edit-user.component';
import {NewUserComponent} from './components/new-user/new-user.component';

const routes: Routes = [
  {
    path: '',
    component: UsersComponent,
  }, {
    path: 'edit',
    component: EditUserComponent
  }, {
    path: 'new',
    component: NewUserComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule {
}
