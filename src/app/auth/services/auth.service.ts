import {Injectable} from '@angular/core';
// import {auth} from 'firebase/app';
// import {User} from 'firebase';
import {AngularFireAuth} from "@angular/fire/compat/auth";
import {first} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(public afAuth: AngularFireAuth) {
  }

  async login(email: string, password: string) {
    let response;
    try {
      response = await this.afAuth.signInWithEmailAndPassword(email, password);
    } catch (e) {
      console.log(e)
    }
    return response;
  }

  async register(email: string, password: string) {
    let response;
    try {
      response = await this.afAuth.createUserWithEmailAndPassword(email, password);
    } catch (e) {
      console.log(e)
    }
    return response;
  }

  async getCurrentUser() {
    return await this.afAuth.authState.pipe(first()).toPromise();
  }

  async logOut() {
    try {
      return await this.afAuth.signOut();
    } catch (e) {
      console.log(e)
    }
  }
}
